#include "classic_menu_viewer.h"
#include <QScreen>
#include <QWindow>
#include <QApplication>

void classic_menu_viewer::showEvent(QShowEvent *e)
{
  if(!property("ChildMenu").toBool())
    {
      QPoint cursor_pos = QCursor::pos();
      int w = window()->windowHandle()->screen()->size().width(),
          h = window()->windowHandle()->screen()->size().height();
      if(cursor_pos.x() + width() > w)
        cursor_pos.setX(cursor_pos.x() - width());
      if(cursor_pos.y() + height() > h)
        cursor_pos.setY(cursor_pos.y() - height());
      move(cursor_pos);
    }
  QMenu::showEvent(e);
}

classic_menu_viewer::classic_menu_viewer(menu *m0, QFont fnt):
  m(0)
{
  setFont(fnt);
  if(m0)
    set_menu(m0);
}

classic_menu_viewer::~classic_menu_viewer()
{
}

void classic_menu_viewer::set_menu(menu *m0)
{
  while(m0 && m0->get_children().size() == 1)
    {
      menu *m1 = dynamic_cast<menu*>(m0->get_children()[0]);
      if(m1)
        m0 = m1;
      else
        break;
    }
  if(m)
    {
      disconnect(m, SIGNAL(update_finished()), this, SLOT(menu_updated()));
      disconnect(this, SIGNAL(aboutToShow()), m, SLOT(update_from_pipe()));
    }
  m = m0;
  if(m)
    {
      connect(m, SIGNAL(update_finished()), SLOT(menu_updated()));
      connect(this, SIGNAL(aboutToShow()), m, SLOT(update_from_pipe()));
      menu_updated();
    }
}

void classic_menu_viewer::menu_updated()
{
  clear();
  for(menu_element *el: m->get_children())
    {
      menu *m1 = dynamic_cast<menu*>(el);
      if(m1)
        {
          classic_menu_viewer *qm = new classic_menu_viewer;
          qm->setIcon(m1->get_icon());
          qm->setTitle(m1->get_caption());
          qm->set_menu(m1);
          qm->setFont(font());
          qm->setProperty("ChildMenu", true);
          addMenu(qm);
        } else
        if(el->is_separator())
          addSection(el->get_icon(), el->get_caption());
        else
          addAction(el->get_icon(), el->get_caption(), el, SLOT(execute()));
    }
}
