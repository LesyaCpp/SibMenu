#ifndef ACTION_UPDATE_H
#define ACTION_UPDATE_H

#include "action.h"

class action_update: public action
{
public:
  action_update();

  void execute(menu_element *);

  inline virtual QString description() const {return "Update root menu";}
};

#endif // ACTION_UPDATE_H
