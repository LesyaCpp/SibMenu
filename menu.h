#ifndef MENU_H
#define MENU_H

#include "menu_element.h"
#include <QVector>
#include <QHash>
#include <QTimer>

class menu: public menu_element
{
  Q_OBJECT

  QHash<QString, menu*> menus;
  QVector<menu_element*> children;
  QString id;
  QString pipe_command;
  inline void execute(){}

  static QTimer* update_timer;
public:
  menu(const QString &caption, const QString &icon, const QString &id, menu *parent=0);
  ~menu();

  inline const QVector<menu_element*>& get_children()const {return children;}
  inline bool is_pipe() {return !pipe_command.isEmpty();}

  static void set_update_interval(int msec);
  inline static int get_update_interval() {return update_timer->interval();}

public slots:
  void update_from_pipe();
  void update();
  void update(QIODevice * const src);

  void clear();
signals:
  void update_finished();
};

#endif // MENU_H
