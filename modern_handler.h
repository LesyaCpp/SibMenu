#ifndef MODERN_HANDLER_H
#define MODERN_HANDLER_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLineEdit>
#include "modern_menu_viewer.h"

class modern_handler : public QWidget
{
  Q_OBJECT

  QVBoxLayout *l;
  QLineEdit *e;
public:
  modern_handler(modern_menu_viewer *v, QWidget *parent = 0);
  ~modern_handler();

protected slots:
  void change_viewer(modern_menu_viewer *target);

  void start_program();
};

#endif // MODERN_HANDLER_H
