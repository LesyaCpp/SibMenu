#ifndef ICONED_LABEL_H
#define ICONED_LABEL_H

#include <QWidget>

class iconed_label : public QWidget
{
  Q_OBJECT
public:
  iconed_label(const QString &text, const QIcon &ico, QWidget *parent = 0);
};

#endif // ICONED_LABEL_H
