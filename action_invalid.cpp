#include "action_invalid.h"
#include <QMessageBox>

action_invalid::action_invalid(const QString &type):
  type(type)
{
}

void action_invalid::execute(menu_element *)
{
  QMessageBox::critical(0, "Invalid action", "Action \""+type+"\" is unsupportted yet.");
}
