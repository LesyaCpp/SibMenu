#ifndef MENU_ELEMENT_H
#define MENU_ELEMENT_H

#include "action.h"

#include <QString>
#include <QVector>
#include <QIcon>

class menu;

class menu_element: public QObject
{
  Q_OBJECT

  friend class menu;
protected:
  menu *parent;

  QString caption;
  QString icon;
  QVector<action*> actions;

  bool sep;

  menu_element(const QString &caption, const QString &icon, menu *parent=0);
  virtual ~menu_element();

public:
  inline bool is_enabled() const {return !actions.isEmpty();}
  inline bool is_separator() const {return sep;}
  inline menu* get_parent() const {return parent;}
  QIcon get_icon() const;
  inline QString get_caption() const {return caption;}

  QString get_description() const;

  static bool prefer_theme_icons;
  static bool disable_icons;

public slots:
  void execute();
};

#endif // MENU_ELEMENT_H
