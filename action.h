#ifndef ACTION_H
#define ACTION_H

#include <QString>

class menu_element;

class action
{
public:
  action() {}
  inline virtual ~action() {}

  virtual void execute(menu_element*) = 0;

  inline virtual QString description() const {return "";}
};

#endif // ACTION_H
