#include "menu_element.h"

bool menu_element::prefer_theme_icons = false;
bool menu_element::disable_icons = false;

menu_element::menu_element(const QString &caption, const QString &icon, menu *parent):
  parent(parent), caption(caption), icon(icon), sep(false)
{
}

menu_element::~menu_element()
{
  for(action *a: actions)
    delete a;
}

QString extract_theme_icon(QString str)
{
  int first_i = str.lastIndexOf("/")+1,
      last_i = str.lastIndexOf(".");

  str = str.mid(first_i, last_i - first_i);
  return str;
}

QIcon menu_element::get_icon() const
{
  if(disable_icons)
    return QIcon();

  QIcon ico;
  if(prefer_theme_icons)
    {
      QString ico_str = extract_theme_icon(icon);
      ico = QIcon::fromTheme(ico_str);
      if(ico.isNull() || ico.pixmap(32, 32).isNull())
        {
          QString icon_theme = QIcon::themeName();
          QIcon::setThemeName("hicolor");
          ico = QIcon::fromTheme(ico_str, QIcon(icon));
          QIcon::setThemeName(icon_theme);
        }
    } else {
      ico = QIcon(icon);
      if(!icon.isEmpty() && (ico.isNull() || ico.pixmap(32, 32).isNull()))
        ico = QIcon::fromTheme(extract_theme_icon(icon));
    }
  return ico;
}

QString menu_element::get_description() const
{
  QString str;
  for(action *a: actions)
    str += a->description()+"\n";
  str.remove(str.size()-1, 1);
  return str;
}

void menu_element::execute()
{
  for(action *a: actions)
    a->execute(this);
}
